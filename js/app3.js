function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users"
    
    //funcion resppuesta peticion
    http.onreadystatechange = function(){
        //validar respuesta
        if(this.status==200 && this.readyState==4){
            let res=document.getElementById('lista');
            const json=JSON.parse(this.responseText);
    
            //ciclo mostrar datos tabla
            for(const datos of json){
                const concPany = datos.company.name 
                + ' - ' + datos.company.bs;
                const concGeo = datos.address.geo.lat + ' - ' + datos.address.geo.lng;
                const concAddress = datos.address.street + ' - ' + datos.address.suite + ' - ' + datos.address.city
                      + ' - ' + datos.address.zipcode + ' - ' + concGeo;
                res.innerHTML+='<tr> <td class="columna1">'+datos.id+'</td>'
                + ' <td class=columna2">'+datos.name+'</td>'
                + ' <td class=columna3">'+datos.username+'</td>'
                + ' <td class=columna4">'+datos.email+'</td>'
                + ' <td class=columna5">'+concAddress+'</td>'
                + ' <td class=columna6">'+datos.phone+'</td>'
                + ' <td class=columna7">'+datos.website+'</td>'
                + ' <td class=columna8">'+concPany+'</td> </tr>'
            }
        } //else alert("surgio un error al hacer la peticion")
    }
    http.open('GET',url,true);
    http.send();
    }
    //codificar botones
    document.getElementById("btnCargar").addEventListener('click',cargarDatos);
    document.getElementById("btnLimpiar").addEventListener('click',function(){
        let res= document.getElementById('lista');
        res.innerHTML="";
    })